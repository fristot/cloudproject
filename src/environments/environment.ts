// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBjvWN7ytnRT_VgRSs1suDmD3dby8qDj3U",
    authDomain: "covid-fa4da.firebaseapp.com",
    databaseURL: "https://covid-fa4da.firebaseio.com",
    projectId: "covid-fa4da",
    storageBucket: "covid-fa4da.appspot.com",
    messagingSenderId: "1015666443849",
    appId: "1:1015666443849:web:c65518a5b8a7d9c5c9a198"
  }
}; 

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
