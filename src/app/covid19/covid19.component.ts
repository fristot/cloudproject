import { Component, OnInit } from '@angular/core';
import { CovidService } from '../covid.service';
import { CoviDataCountry } from '../coviDataCountry.model';
import { CoviDataSummary } from '../coviDataSummary.model';
import { User } from '../user.model';

@Component({
  selector: 'app-covid19',
  templateUrl: './covid19.component.html',
  styleUrls: ['./covid19.component.css']
})
export class Covid19Component implements OnInit {

  user: User;

  constructor(public covidService: CovidService) { }

  ngOnInit(): void {
    this.user = this.covidService.getUser();
  }
}
