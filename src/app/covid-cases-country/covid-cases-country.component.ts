import { Component, OnInit } from '@angular/core';
import { CovidService } from '../covid.service';
import { CoviDataCountry } from '../coviDataCountry.model';
import {Sort} from '@angular/material/sort';
import { Router } from '@angular/router';

@Component({
  selector: 'app-covid-cases-country',
  templateUrl: './covid-cases-country.component.html',
  styleUrls: ['./covid-cases-country.component.css']
})
export class CovidCasesCountryComponent implements OnInit {

  countries: CoviDataCountry[];

  constructor(public covidService: CovidService,public router:Router) {  }

  ngOnInit(): void {
    this.covidService.getCountries().subscribe((data: CoviDataCountry[])=>{
      this.countries = data
    });
    console.log(this.countries)
  }

  public countryPage(slug: string){
    // once a day if it is 1st time
    this.covidService.apiRequestCountryTotal(slug)
    this.router.navigate(["country/"+slug]);
    console.log("navigate to country/"+slug);
  }
}


