import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidCasesCountryComponent } from './covid-cases-country.component';

describe('CovidCasesCountryComponent', () => {
  let component: CovidCasesCountryComponent;
  let fixture: ComponentFixture<CovidCasesCountryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CovidCasesCountryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidCasesCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
