export class News {
    Title: string;
    Description: string;
    ImageLink: string;
    ReadMore: string;
    Country: string;
    Author: string;
    Date: string;

    constructor(
        Title: string,
        Description: string,
        ImageLink: string,
        ReadMore: string,
        Country: string,
        Author: string,
        Date: string){
            this.Title = Title;
            this.Description = Description;
            this.ImageLink = ImageLink;
            this.ReadMore = ReadMore;
            this.Country = Country;
            this.Author = Author;
            this.Date = Date;
        }
};