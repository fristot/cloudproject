import { Component, OnInit } from '@angular/core';

import { CovidService } from '../covid.service';
import { CoviDataSummary } from '../coviDataSummary.model';

@Component({
  selector: 'app-covid-summary',
  templateUrl: './covid-summary.component.html',
  styleUrls: ['./covid-summary.component.css']
})
export class CovidSummaryComponent implements OnInit {

  coviDataSummary: CoviDataSummary;

  constructor(public covidService: CovidService) { }

  ngOnInit(): void {
    this.covidService.getSummary().subscribe((data: CoviDataSummary) => {
      this.coviDataSummary = data
    });
  }

}
