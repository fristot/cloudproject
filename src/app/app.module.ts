import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import { ChartsModule } from 'ng2-charts';

import { SigninComponent } from './signin/signin.component';
import { Covid19Component } from './covid19/covid19.component';
import { CovidCasesDistributionComponent } from './covid-cases-distribution/covid-cases-distribution.component';
import { CovidCasesDailyComponent } from './covid-cases-daily/covid-cases-daily.component';
import { CovidCasesCountryComponent } from './covid-cases-country/covid-cases-country.component';
import { CovidSummaryComponent } from './covid-summary/covid-summary.component';
import { CovidCountryPageComponent } from './covid-country-page/covid-country-page.component';
import { CovidCountrySummaryComponent } from './covid-country-summary/covid-country-summary.component';
import { CovidCountryDistributionComponent } from './covid-country-distribution/covid-country-distribution.component';
import { CovidCountryDailyComponent } from './covid-country-daily/covid-country-daily.component';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatMenuModule } from '@angular/material/menu';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NewsFeatureComponent } from './news-feature/news-feature.component';
import { AddNewsComponent } from './add-news/add-news.component';
import { NewsFeatureCountryComponent } from './news-feature-country/news-feature-country.component';


@NgModule({
  declarations: [
    AppComponent,
    SigninComponent,
    Covid19Component,
    CovidCasesDistributionComponent,
    CovidCasesDailyComponent,
    CovidCasesCountryComponent,
    CovidSummaryComponent,
    CovidCountryPageComponent,
    CovidCountrySummaryComponent,
    CovidCountryDistributionComponent,
    CovidCountryDailyComponent,
    NewsFeatureComponent,
    AddNewsComponent,
    NewsFeatureCountryComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    ChartsModule,
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatMenuModule,
    MatCardModule,
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
