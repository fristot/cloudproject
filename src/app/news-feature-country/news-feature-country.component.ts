import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CovidService } from '../covid.service';
import { News } from '../news.model';

@Component({
  selector: 'app-news-feature-country',
  templateUrl: './news-feature-country.component.html',
  styleUrls: ['./news-feature-country.component.css']
})
export class NewsFeatureCountryComponent implements OnInit {

  slug: string;
  newsList: Array<News> = [];
  done: boolean = false

  constructor(public covidService: CovidService,private router : Router) { }

  ngOnInit(): void {
    var n = this.router.url.toString().split('/').length
    this.slug = this.router.url.toString().split('/')[n-1];
    this.covidService.getNews().subscribe((data: News[]) => {
      for(let news of data){
        if(news.Country == this.slug && !this.done){
          this.newsList.push(news)
        }
      }
      this.done = true
    });
  }
}
