import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NewsFeatureCountryComponent } from './news-feature-country.component';

describe('NewsFeatureCountryComponent', () => {
  let component: NewsFeatureCountryComponent;
  let fixture: ComponentFixture<NewsFeatureCountryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NewsFeatureCountryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NewsFeatureCountryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
