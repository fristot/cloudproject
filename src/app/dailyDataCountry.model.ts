export interface DailyDataCountry {
    Country: string;
    Confirmed: number;
    Deaths: number;
    Recovered: number;
    Date:string;
};
