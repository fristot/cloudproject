import { collectExternalReferences } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CovidService } from '../covid.service';
import { User } from '../user.model';

@Component({
  selector: 'app-covid-country-page',
  templateUrl: './covid-country-page.component.html',
  styleUrls: ['./covid-country-page.component.css']
})
export class CovidCountryPageComponent implements OnInit {

  user: User;
  slug: string;

  constructor(private router : Router,public covidService: CovidService) { }

  ngOnInit(): void {
    this.user = this.covidService.getUser();
    var n = this.router.url.toString().split('/').length
    this.slug = this.router.url.toString().split('/')[n-1].toUpperCase();
  }
}
