export interface CoviDataSummary {
    totalCases: number;
    newCases: number;
    activeCases: number;
    totalRecovered: number;
    newRecovered: number;
    recoveryRate: number;
    totalDeaths: number;
    newDeaths: number;
    mortalityRate: number;
    date: string};