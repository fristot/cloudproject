import { Component, OnInit } from '@angular/core';
import { count } from 'rxjs/operators';
import { CovidService } from '../covid.service';
import { News } from '../news.model';

@Component({
  selector: 'app-news-feature',
  templateUrl: './news-feature.component.html',
  styleUrls: ['./news-feature.component.css']
})
export class NewsFeatureComponent implements OnInit {

  newsList: Array<News> = []
  countryTag: string = "worldwild"
  done: boolean = false

  constructor(public covidService: CovidService) { }

  ngOnInit(): void {
    this.covidService.getNews().subscribe((data: News[]) => {
      for(let news of data){
        if(news.Country == this.countryTag && !this.done){
          this.newsList.push(news)
        }
      }
      this.done = true
    });
  }
}
