import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import * as moment from 'moment';
import { BaseChartDirective, Label } from 'ng2-charts';
import { CovidService } from '../covid.service';
import { DailyData } from '../dailyData.model';
import { DailyDataCountry } from '../dailyDataCountry.model';

@Component({
  selector: 'app-covid-country-daily',
  templateUrl: './covid-country-daily.component.html',
  styleUrls: ['./covid-country-daily.component.css']
})
export class CovidCountryDailyComponent implements OnInit {

  totalData: DailyDataCountry[];
  slug: string;
  name: string;
  done: boolean = false;
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Deaths' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Recovered' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily New Cases' }
  ];


  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Total Deaths' },
    { data: [], label: 'Total Recovered' },
    { data: [], label: 'Total Cases'}
  ];
  lineChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
          display: false,
      },
  }
  };
  lineChartLabels: Label[] = [];
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(public covidService: CovidService,private router : Router) { }

  ngOnInit(): void {
    if(!this.done){
      var n = this.router.url.toString().split('/').length
      this.slug = this.router.url.toString().split('/')[n-1];
      for (let j = 0; j < 7; j++) {
        this.barChartLabels[j] = moment().subtract('days', 6-j).format('MMM Do');
      }
      this.covidService.getTotalCountry(this.slug).subscribe((data: DailyDataCountry[])=>{
        this.totalData = data 
        if(this.totalData.length > 100){
          this.name = this.totalData[0].Country;
          var n = this.totalData.length
          for (let j = 0; j < 7; j++) {
            this.barChartData[0].data[j] = Math.max(this.totalData[n-8+j].Deaths - this.totalData[n-9+j].Deaths,0);
            this.barChartData[1].data[j] = Math.max(this.totalData[n-8+j].Recovered - this.totalData[n-9+j].Recovered,0);
            this.barChartData[2].data[j] = Math.max(this.totalData[n-8+j].Confirmed - this.totalData[n-9+j].Confirmed,0);
          }
          for (let j = 0; j < this.totalData.length; j++) {
            this.lineChartData[0].data[j] = this.totalData[j].Deaths;
            this.lineChartData[1].data[j] = this.totalData[j].Recovered;
            this.lineChartData[2].data[j] = this.totalData[j].Confirmed;
            this.lineChartLabels[j] = moment().subtract('days', this.totalData.length-j).format('MMM Do');
          }
          this.chart.update();
          this.done = true;
        }
      })
      console.log(this.slug +" daily data data loaded")
    }
  }


  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }


}
