import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidCountryDailyComponent } from './covid-country-daily.component';

describe('CovidCountryDailyComponent', () => {
  let component: CovidCountryDailyComponent;
  let fixture: ComponentFixture<CovidCountryDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CovidCountryDailyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidCountryDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
