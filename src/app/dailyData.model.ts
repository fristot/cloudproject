export interface DailyData {
    NewCases: number;
    TotalCases: number;
    NewDeaths: number;
    TotalDeaths: number;
    NewRecovered: number;
    TotalRecovered: number;
    id: number;
};
