import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidCasesDailyComponent } from './covid-cases-daily.component';

describe('CovidCasesDailyComponent', () => {
  let component: CovidCasesDailyComponent;
  let fixture: ComponentFixture<CovidCasesDailyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CovidCasesDailyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidCasesDailyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
