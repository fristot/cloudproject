import { Component, OnInit, ViewChild } from '@angular/core';
import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import * as moment from 'moment';
import { BaseChartDirective, Label } from 'ng2-charts';
import { CovidService } from '../covid.service';
import { DailyData } from '../dailyData.model';

@Component({
  selector: 'app-covid-cases-daily',
  templateUrl: './covid-cases-daily.component.html',
  styleUrls: ['./covid-cases-daily.component.css']
})
export class CovidCasesDailyComponent implements OnInit {

  totalData: DailyData[];
  done: boolean;
  public barChartOptions: ChartOptions = {
    responsive: true,
    // We use these empty structures as placeholders for dynamic theming.
    scales: { xAxes: [{}], yAxes: [{}] },
    plugins: {
      datalabels: {
        anchor: 'end',
        align: 'end',
      }
    }
  };
  public barChartLabels: Label[] = ['N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'];
  public barChartType: ChartType = 'bar';
  public barChartLegend = true;
  public barChartPlugins = [pluginDataLabels];

  public barChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Deaths' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily Recovered' },
    { data: [0, 0, 0, 0, 0, 0, 0], label: 'Daily New Cases' }
  ];


  lineChartData: ChartDataSets[] = [
    { data: [], label: 'Total Deaths' },
    { data: [], label: 'Total Recovered' },
    { data: [], label: 'Total Cases' }
  ];
  lineChartOptions: ChartOptions = {
    responsive: true,
    plugins: {
      datalabels: {
        display: false,
      },
    }
  };
  lineChartLabels: Label[] = [];
  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType: ChartType = 'line';

  @ViewChild(BaseChartDirective, { static: true }) chart: BaseChartDirective;

  constructor(public covidService: CovidService) { }

  ngOnInit(): void {
    if (!this.done) {
      for (let j = 0; j < 7; j++) {
        this.barChartLabels[j] = moment().subtract('days', 6 - j).format('MMM Do');
      }
      this.covidService.getTotal().subscribe((data: DailyData[]) => {
        this.totalData = data
        console.log(this.totalData)
        if (this.totalData.length > 100) {
          var n = this.totalData.length
          this.totalData.sort(function (a, b) {
            return a.TotalCases - b.TotalCases;
          });
          for (let j = 0; j < 7; j++) {
            if (this.totalData[n - 8 + j].id != null) {
              this.barChartData[0].data[j] = this.totalData[n - 8 + j].NewDeaths;
              this.barChartData[1].data[j] = this.totalData[n - 8 + j].NewRecovered;
              this.barChartData[2].data[j] = this.totalData[n - 8 + j].NewCases;
            }
          }
          for (let j = 0; j < this.totalData.length; j++) {
            if (this.totalData[j].id != null) {
              this.lineChartData[0].data[j] = this.totalData[j].TotalDeaths;
              this.lineChartData[1].data[j] = this.totalData[j].TotalRecovered;
              this.lineChartData[2].data[j] = this.totalData[j].TotalCases;
              this.lineChartLabels[j] = moment().subtract('days', this.totalData.length - j).format('MMM Do');
            }
          }
          this.chart.update();
          this.done = true;
        }
      })
    }
  }


  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }


}
