import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';


import { CovidService } from '../covid.service';
import { CoviDataSummary } from '../coviDataSummary.model';


@Component({
  selector: 'app-covid-cases-distribution',
  templateUrl: './covid-cases-distribution.component.html',
  styleUrls: ['./covid-cases-distribution.component.scss']
})
export class CovidCasesDistributionComponent implements OnInit {

  coviDataSummary: CoviDataSummary;

  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Died Cases','Recovered Cases' , 'Active Cases'];
  public pieChartData: number[] = [300, 500, 100]; //-----------TBD------------
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(137, 196, 244, 1)', 'rgba(255, 223, 126, 0.8)'],
    },
  ];

  constructor(public covidService: CovidService) { }

  ngOnInit(): void {
    this.covidService.getSummary().subscribe((data: CoviDataSummary)=>{
      this.coviDataSummary = data
      this.pieChartData = [
        this.coviDataSummary.totalDeaths,
        this.coviDataSummary.totalRecovered,
        this.coviDataSummary.activeCases];
    });
    
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
}