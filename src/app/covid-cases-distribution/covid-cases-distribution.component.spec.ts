import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidCasesDistributionComponent } from './covid-cases-distribution.component';

describe('CovidCasesDistributionComponent', () => {
  let component: CovidCasesDistributionComponent;
  let fixture: ComponentFixture<CovidCasesDistributionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CovidCasesDistributionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidCasesDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
