import { Injectable } from '@angular/core';
import firebase from 'firebase/app';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from './user.model';
import { CoviDataSummary } from './coviDataSummary.model'
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClient } from '@angular/common/http';
import { CoviDataCountry } from './coviDataCountry.model';
import { CoviDataCountryRaw } from './coviDataCountryRaw.model';
import { DailyData } from './dailyData.model';
import { DailyDataCountry } from './dailyDataCountry.model';
import { News } from './news.model';


@Injectable({
  providedIn: 'root'
})


export class CovidService {
  
  user: User;
  coviDataSummary: CoviDataSummary;
  coviDataCountriesRaw: Array<CoviDataCountryRaw> = [];
  coviDataCountries: Array<CoviDataCountry> = [];
  totalData: Array<DailyData> = [];
  totalDataCountry: Array<DailyDataCountry> = [];


  constructor(private afAuth: AngularFireAuth, 
    private router: Router,
    private firestore: AngularFirestore,
    private http: HttpClient) { }

  async signInWithGoogle(){
    const credentals = await this.afAuth.signInWithPopup(new firebase.auth.GoogleAuthProvider());
    this.user = {
      uid: credentals.user.uid,
      displayName: credentals.user.displayName,
      email: credentals.user.email
    };
    localStorage.setItem("users", JSON.stringify(this.user));
    this.updateUserData();
    this.router.navigate(["covid19"]);
  }

  private updateUserData(){
    this.firestore.collection("users").doc(this.user.uid).set({
      uid: this.user.uid,
      displayName: this.user.displayName,
      email: this.user.email
    },{merge: true})
  }

  getUser(){
    if(this.user == null && this.userSignedIn){
      this.user = JSON.parse(localStorage.getItem("users"));
    }
    return this.user;
  }

  userSignedIn(): boolean{
    return JSON.parse(localStorage.getItem("users")) != null;
  }

  signOut(){
    this.afAuth.signOut();
    localStorage.removeItem("users");
    this.user = null;
    this.router.navigate(["signin"]);
  }

  apiRequestSummary(){
    if(!this.updatedSummary()){
      // once a day : 
      this.http.get<JSON>("https://api.covid19api.com/summary").subscribe(data => {
      this.coviDataSummary = {
        totalCases: data['Global']['TotalConfirmed'],
        newCases: data['Global']['NewConfirmed'],
        activeCases: data['Global']['TotalConfirmed']-data['Global']['TotalRecovered']-data['Global']['TotalDeaths'],
        totalRecovered: data['Global']['TotalRecovered'],
        newRecovered: data['Global']['NewRecovered'],
        recoveryRate: Math.round(data['Global']['TotalRecovered']/data['Global']['TotalConfirmed']*10000)/100,
        totalDeaths: data['Global']['TotalDeaths'],
        newDeaths: data['Global']['NewDeaths'],
        mortalityRate: Math.round(data['Global']['TotalDeaths']/data['Global']['TotalConfirmed']*10000)/100,
        date: data['Date']};
      this.coviDataCountriesRaw = data['Countries'];
      console.log(this.coviDataSummary);
      console.log(this.coviDataCountriesRaw);
      console.log("summary api -> firestore -> local")
      console.log("countries api -> firestore")
      localStorage.setItem("summary", JSON.stringify(this.coviDataSummary));
      this.updateSummary();
      this.updateCountryData();
    });  
    } else {
      console.log("Data up to date")
    }
    
    
    
  }

  apiRequestTotal(){
    this.http.get<DailyData[]>('https://api.covid19api.com/world?from=2020-04-13T00:00:00Z').subscribe((total) => {
      this.totalData = total;
      var n = 0;
      this.totalData.forEach(data => data.id = n++)
      console.log(this.totalData)
      localStorage.setItem("total", JSON.stringify(this.totalData));
      this.updateTotal();
      });
  }

  apiRequestCountryTotal(slug: string){
    this.http.get<DailyDataCountry[]>("https://api.covid19api.com/total/country/"+slug+"?from=2020-04-13T00:00:00Z").subscribe((total) => {
      this.totalDataCountry = total;
      console.log(this.totalDataCountry)
      localStorage.setItem(slug, JSON.stringify(this.totalDataCountry));
      this.updateCountryTotal(slug);
    });
  }
  

  getSummary(){
    console.log("summary firestore access")
    return this.firestore.collection("summary").doc("summary").valueChanges()
  }

  getCountries(){
    console.log("countries summary firestore access")
    return this.firestore.collection('countries').valueChanges()
  }

  getTotal(){
    console.log("worldilde daily firestore access")
    return this.firestore.collection('total').valueChanges()
  }

  getTotalCountry(slug: string){
    console.log(slug +" daily firestore access")
    return this.firestore.collection("countries").doc(slug).collection("total").valueChanges()
  }

  private updateSummary(){
    this.firestore.collection("summary").doc("summary").set({
      totalCases: this.coviDataSummary.totalCases,
      newCases: this.coviDataSummary.newCases,
      activeCases: this.coviDataSummary.activeCases,
      totalRecovered: this.coviDataSummary.totalRecovered,
      newRecovered: this.coviDataSummary.newRecovered,
      recoveryRate: this.coviDataSummary.recoveryRate,
      totalDeaths: this.coviDataSummary.totalDeaths,
      newDeaths: this.coviDataSummary.newDeaths,
      mortalityRate: this.coviDataSummary.mortalityRate,
      date: this.coviDataSummary.date
    },{merge: true});
  }

  private updateCountryData(){
    this.coviDataCountriesRaw.forEach(country => {
      if(country != null){
        this.firestore.collection("countries").doc(country.Slug).set({
          name: country.Country,
          slug: country.Slug,
          newConfirmed: country.NewConfirmed,
          totalConfirmed: country.TotalConfirmed,
          newDeaths: country.NewDeaths,
          totalDeaths: country.TotalDeaths,
          newRecovered: country.NewRecovered,
          totalRecovered: country.TotalRecovered,
          date: country.Date},{merge: true});
        }
    });
  }

  private updateTotal(){
    this.totalData.forEach((dailyData: DailyData) => {
      if(dailyData != null){
        this.firestore.collection("total").doc(this.myToString(dailyData.id)).set({
          NewCases: dailyData.NewCases,
          NewDeaths: dailyData.NewDeaths,
          NewRecovered: dailyData.NewRecovered,
          TotalCases: dailyData.TotalCases,
          TotalDeaths: dailyData.TotalDeaths,
          TotalRecovered: dailyData.TotalRecovered,
          id: dailyData.id
        },{merge: true});
      }
    });
  }

  private updateCountryTotal(slug: string){
    this.totalDataCountry.forEach((dailyData: DailyDataCountry) => {
      if(dailyData != null){
        this.firestore.collection("countries").doc(slug).collection("total").doc(dailyData.Date).set({
          Country: dailyData.Country,
          Confirmed: dailyData.Confirmed,
          Deaths: dailyData.Deaths,
          Recovered: dailyData.Recovered,
          Date:dailyData.Date,
        },{merge: true});
      }
    });
  }

  addNews(news: News) {
    this.firestore.collection("news").add(news);
  }

  getNews() {
    return this.firestore.collection("news").valueChanges()
  }

  updatedSummary(){
    this.firestore.collection("summary").doc("summary").valueChanges().subscribe((summary) => {
      var a = (new Date(summary["date"])).getMonth()*100 + (new Date(summary["date"])).getDate()
      var b = (new Date().getMonth())*100 + (new Date()).getDate()
      return (a == b)
    })
    return false
  }

  updatedCountry(slug: string){

  }


  myToString(n: number){
    if (n<10){
      return '00'+n.toString();
    }
    if (n<100){
      return '0'+n.toString();
    }
    return n.toString();
  }
}
