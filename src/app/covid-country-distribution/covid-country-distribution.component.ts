
import { Component, OnInit } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';


import { CovidService } from '../covid.service';
import { CoviDataSummary } from '../coviDataSummary.model';
import { Router } from '@angular/router';
import { CoviDataCountry } from '../coviDataCountry.model';


@Component({
  selector: 'app-covid-country-distribution',
  templateUrl: './covid-country-distribution.component.html',
  styleUrls: ['./covid-country-distribution.component.css']
})
export class CovidCountryDistributionComponent implements OnInit {

  coviDataCountry: CoviDataCountry;
  slug: string;
  name: string;

  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  public pieChartLabels: Label[] = ['Died Cases','Recovered Cases' , 'Active Cases'];
  public pieChartData: number[] = [300, 500, 100]; //-----------TBD------------
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [pluginDataLabels];
  public pieChartColors = [
    {
      backgroundColor: ['rgba(255,0,0,0.3)', 'rgba(137, 196, 244, 1)', 'rgba(255, 223, 126, 0.8)'],
    },
  ];

  constructor(public covidService: CovidService,private router : Router) { }

  ngOnInit(): void {
    var n = this.router.url.toString().split('/').length;
    this.slug = this.router.url.toString().split('/')[n-1];
    this.covidService.getCountries().subscribe((data: CoviDataCountry[])=>{
      data.forEach(data => {
        if(data.slug ===this.slug){
          this.coviDataCountry = data;
          this.name = data.name;
        }
      });
      this.pieChartData = [
        this.coviDataCountry.totalDeaths,
        this.coviDataCountry.totalRecovered,
        this.coviDataCountry.totalConfirmed - this.coviDataCountry.totalRecovered];
    })
    
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
}