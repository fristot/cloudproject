import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidCountryDistributionComponent } from './covid-country-distribution.component';

describe('CovidCountryDistributionComponent', () => {
  let component: CovidCountryDistributionComponent;
  let fixture: ComponentFixture<CovidCountryDistributionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CovidCountryDistributionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidCountryDistributionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
