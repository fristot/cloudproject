
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { CovidService } from '../covid.service';
import { CoviDataCountry } from '../coviDataCountry.model';
import { CoviDataSummary } from '../coviDataSummary.model';

@Component({
  selector: 'app-covid-country-summary',
  templateUrl: './covid-country-summary.component.html',
  styleUrls: ['./covid-country-summary.component.css']
})
export class CovidCountrySummaryComponent implements OnInit {

  coviDataCountry: CoviDataCountry;
  coviDataSummary: CoviDataSummary;
  slug: String;
  name: String;

  constructor(public covidService: CovidService,private router : Router) { }

  ngOnInit(): void {
    var n = this.router.url.toString().split('/').length;
    this.slug = this.router.url.toString().split('/')[n-1];
    this.covidService.getCountries().subscribe((data: CoviDataCountry[]) => {
      data.forEach(data => {
        if(data.slug === this.slug){
          this.coviDataSummary = {
            totalCases: data.totalConfirmed,
            newCases: data.newConfirmed,
            activeCases: data.totalConfirmed-data.totalRecovered-data.totalDeaths,
            totalRecovered: data.totalRecovered,
            newRecovered: data.newRecovered,
            recoveryRate: Math.round(data.totalRecovered/data.totalConfirmed*10000)/100,
            totalDeaths: data.totalDeaths,
            newDeaths: data.newDeaths,
            mortalityRate: Math.round(data.totalDeaths/data.totalConfirmed*10000)/100,
            date: "N/A"
          }
          this.name = data.name;
        }
    })
    });
  }
}
