import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CovidCountrySummaryComponent } from './covid-country-summary.component';

describe('CovidCountrySummaryComponent', () => {
  let component: CovidCountrySummaryComponent;
  let fixture: ComponentFixture<CovidCountrySummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CovidCountrySummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CovidCountrySummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
