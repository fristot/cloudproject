import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { CovidCountryPageComponent } from './covid-country-page/covid-country-page.component';
import { Covid19Component } from './covid19/covid19.component';
import { SecurePagesGuard } from './secure-pages.guard';
import { SigninComponent } from './signin/signin.component';

const routes: Routes = [
  {path: "signin", component: SigninComponent,canActivate: [SecurePagesGuard]},
  {path: "covid19", component: Covid19Component,canActivate: [AuthGuard]},
  {path:"country/:slug", component: CovidCountryPageComponent,canActivate: [AuthGuard]},
  {path: "", pathMatch: "full", redirectTo: "singnin"},
  {path: "**", redirectTo: "signin"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
